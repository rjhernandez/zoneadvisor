## import necessary modules
Import-Module "${env:ProgramFiles(x86)}\Microsoft Visual Studio 14.0\Common7\IDE\Extensions\Microsoft\Web Tools\Publish\Scripts\1.0.1\publish-module.psm1"

## clean up
rm .\.release\ -Recurse -Force -ErrorAction Ignore

## build api package
dotnet publish .\src\ZoneAdvisor.Api\project.json -o .\.release\api --no-source --runtime active
dotnet publish .\src\ZoneAdvisor\project.json -o \.release\client --no-source --runtime active

## publish to Azure
Publish-AspNet -packOutput .\.release\api -publishProperties @{
    'WebPublishMethod'='MSDeploy';
    'MSDeployServiceURL'='zoneadvisorapi.scm.azurewebsites.net:443';
		'DeleteExistingFiles'=$true;
		'SkipExtraFilesOnServer'=$false;
    'DeployIisAppPath'='zoneadvisorapi';
    'Username'='$zoneadvisorapi';
    'Password'="aXYbRMzGycKlN68Fu6vjvpJf01T40pKqdt3rE5KW33YlglHqARTTDkjoriNa"
  }

## publish to Azure
Publish-AspNet -packOutput .\.release\api -publishProperties @{
    'WebPublishMethod'='MSDeploy';
    'MSDeployServiceURL'='zoneadvisor.scm.azurewebsites.net:443';
		'DeleteExistingFiles'=$true;
		'SkipExtraFilesOnServer'=$false;
    'DeployIisAppPath'='zoneadvisor';
    'Username'='$zoneadvisor';
    'Password'="aXYbRMzGycKlN68Fu6vjvpJf01T40pKqdt3rE5KW33YlglHqARTTDkjoriNa"
  }
