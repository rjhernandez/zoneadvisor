﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;
using ZoneAdvisor.Common;

namespace ZoneAdvisor.Earthquakes
{
    public class Usgs : HttpQuery, IUsgs
    {
        public Usgs(EarthquakeSettings earthquakeSettings) : base(earthquakeSettings.Usgs)
        {
        }

        public async Task<FeatureCollection> Query(params IUsgsCriteria[] criteria)
        {
            return await DoGet<FeatureCollection>("query", criteria);
        }

        protected override IDictionary<string, object> GetDefaultCriteria()
        {
            return new Dictionary<string, object>
            {
                { "format", "geojson" },
            };
        }

    }
}