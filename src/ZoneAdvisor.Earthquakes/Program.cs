﻿using System;
using System.Linq;

namespace ZoneAdvisor.Earthquakes
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // Just a Sample Query.
            OutputRecentQuakes().Wait();

            Console.WriteLine("Press any key to continue ... [ENTER]");
            Console.ReadKey();
        }
        
        private static async System.Threading.Tasks.Task OutputRecentQuakes()
        {
            var settings = new EarthquakeSettings { Usgs = "http://earthquake.usgs.gov/fdsnws/event/1/" };
            var usgs = new Usgs(settings);
            foreach (var quake in (await usgs.Query()).Features)
            {
                Console.WriteLine($"QUAKE: {quake.Properties["title"]}");
            }
        }
    }
}
