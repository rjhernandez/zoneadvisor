﻿using System.Collections;
using System.Collections.Generic;
using ZoneAdvisor.Common;
using ZoneAdvisor.Common.Models;

namespace ZoneAdvisor.Earthquakes.Models
{
    public class Radius : IUsgsCriteria
    {
        public LatLon LatLon { get; set; }
        public decimal RadiusKilometers { get; set; }

        public bool IsValid()
        {
            return LatLon != null;
        }

        public IDictionary GetCriteria()
        {
            if (LatLon == null)
                return new Dictionary<string, object>();

            return new Dictionary<string, object>
            {
                { "latitude", LatLon.Latitude},
                { "longitude", LatLon.Longitude},
                { "maxradiuskm",  RadiusKilometers},
            };
        }
    }
}
