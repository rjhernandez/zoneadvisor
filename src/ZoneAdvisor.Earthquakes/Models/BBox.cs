﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using ZoneAdvisor.Common;

namespace ZoneAdvisor.Earthquakes.Models
{
    public class BBox : IUsgsCriteria
    {
        public decimal? MinLatitude { get; set; }
        public decimal? MaxLatitude { get; set; }
        public decimal? MinLongitude { get; set; }
        public decimal? MaxLongitude { get; set; }

        public bool IsValid()
        {
            return true;
        }

        public IDictionary GetCriteria()
        {
            var @params = new Dictionary<string, object>();

            if (MinLatitude.HasValue)
                @params.Add("minlatitude", MinLatitude.Value);
            if (MaxLatitude.HasValue)
                @params.Add("maxlatitude", MaxLatitude.Value);
            if (MinLongitude.HasValue)
                @params.Add("minlongitude", MinLongitude.Value);
            if (MaxLongitude.HasValue)
                @params.Add("maxlatitude", MaxLongitude.Value);

            return @params;
        }
    }
}