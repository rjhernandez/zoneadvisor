﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ZoneAdvisor.Common;

namespace ZoneAdvisor.Earthquakes.Models
{
    public class Magnitude : IUsgsCriteria
    {
        private static readonly IDictionary All = new ReadOnlyDictionary<string, object>(
            new Dictionary<string, object>
            {
                {"includeallmagnitudes", true}
            });

    public MagnitudeCriteria MagnitudeCriteria { get; set; }

        public bool IsValid()
        {
            return true;
        }

        public IDictionary GetCriteria()
        {
            if (MagnitudeCriteria == MagnitudeCriteria.All)
                return All;

            return new Dictionary<string, object>
            {
                {"minmagnitude", (int) MagnitudeCriteria}
            };
        }
    }
}