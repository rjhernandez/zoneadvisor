﻿namespace ZoneAdvisor.Earthquakes.Models
{
    public enum MagnitudeCriteria
    {
        All,
        One,
        Two,
        Three,
        Four,
        Five
    }
}