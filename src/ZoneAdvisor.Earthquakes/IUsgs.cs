﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;

namespace ZoneAdvisor.Earthquakes
{
    public interface IUsgs
    {
        Task<FeatureCollection> Query(params IUsgsCriteria[] criteria);
    }
}