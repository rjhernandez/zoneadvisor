﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ZoneAdvisor.Common.DbClient
{
    public interface IDocumentDbClient<TType>
    {
        Task<IEnumerable<TType>> Read(Expression<Func<TType, bool>> filter = null);

        Task Upsert(IEnumerable<TType> items);        
    }
}