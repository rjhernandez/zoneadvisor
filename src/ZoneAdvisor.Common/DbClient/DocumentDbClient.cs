﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace ZoneAdvisor.Common.DbClient
{
    public class DocumentDbClient<TType> : IDocumentDbClient<TType>
    {
        private readonly DocumentClient _client;
        private readonly string _dbName;
        private readonly string _collectionName;
        private bool _initialized = false;

        public DocumentDbClient(
            string host,
            string authKey,
            string dbName,
            string collectionName)
        {
            this._client = new DocumentClient(new Uri(host), authKey);
            this._dbName = dbName;
            this._collectionName = collectionName;
        }

        public async Task Initialize()
        {
            if (_initialized)
                return;

            await CreateDatabaseIfNotExistsAsync(_dbName);
            await CreateCollectionIfNotExistsAsync(_dbName, _collectionName);
            _initialized = true;
        }

        public async Task<IEnumerable<TType>> Read(Expression<Func<TType, bool>> filter = null)
        {
            await Initialize();
            var query =  _client
                .CreateDocumentQuery<TType>(
                    UriFactory.CreateDocumentCollectionUri(_dbName, _collectionName));

            if (filter != null) 
                return query
                    .Where(filter)
                    .ToList();

            return query.ToList();
        }
        
        public async Task Upsert(IEnumerable<TType> items)
        {
            await Initialize();
            var index = 0;
            foreach (var item in items)
            {
                await _client
                    .UpsertDocumentAsync(
                        UriFactory.CreateDocumentCollectionUri(_dbName, _collectionName), item);
                Console.WriteLine($"[{index:D5}] Created '{_collectionName}' Document");
                ++index;
            }
                
        }

        private async Task CreateDatabaseIfNotExistsAsync(string dbName)
        {
            try
            {
                await _client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(dbName));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await _client.CreateDatabaseAsync(new Database { Id = dbName });
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task CreateCollectionIfNotExistsAsync(string dbName, string collectionName)
        {
            try
            {
                await _client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(dbName, collectionName));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await _client.CreateDocumentCollectionAsync(
                        UriFactory.CreateDatabaseUri(dbName),
                        new DocumentCollection { Id = collectionName },
                        new RequestOptions { OfferThroughput = 1000 });
                }
                else
                {
                    throw;
                }
            }
        }
    }
}
