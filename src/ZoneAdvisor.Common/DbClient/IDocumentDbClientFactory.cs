﻿using System.Threading.Tasks;

namespace ZoneAdvisor.Common.DbClient
{
    public interface IDocumentDbClientFactory
    {
        Task<IDocumentDbClient<TType>> Create<TType>(string dbName, string collectionName);
    }
}
