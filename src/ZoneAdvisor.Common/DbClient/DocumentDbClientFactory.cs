﻿using System.Threading.Tasks;

namespace ZoneAdvisor.Common.DbClient
{
    public class DocumentDbClientFactory : IDocumentDbClientFactory
    {
        private readonly string _host;
        private readonly string _authKey;

        public DocumentDbClientFactory(
            string host,
            string authKey)
        {
            _host = host;
            _authKey = authKey;
        }

        public async Task<IDocumentDbClient<TType>> Create<TType>(string dbName, string collectionName)
        {
            var client = new DocumentDbClient<TType>(_host, _authKey, dbName, collectionName);
            await client.Initialize();
            return client;
        }
    }
}