﻿namespace ZoneAdvisor.Common.Models
{
    public class LatLon
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}