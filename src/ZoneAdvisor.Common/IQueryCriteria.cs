﻿using System.Collections;

namespace ZoneAdvisor.Common
{
    public interface IQueryCriteria
    {
        bool IsValid();
        IDictionary GetCriteria();
    }
}