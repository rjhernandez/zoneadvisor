﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Flurl;
using Flurl.Http;
using System.Threading.Tasks;

namespace ZoneAdvisor.Common
{
    public abstract class HttpQuery
    {
        protected abstract IDictionary<string, object> GetDefaultCriteria();

        protected string Uri { get; }

        protected HttpQuery(
            string uri)
        {
            Uri = uri;
        }

        protected async Task<TType> DoGet<TType>(string method, IEnumerable<IQueryCriteria> queryCriteria = null)
        {
            var request = Uri.SetQueryParams(GetDefaultCriteria());

            queryCriteria = queryCriteria ?? Enumerable.Empty<IQueryCriteria>();

            queryCriteria
                .Where(criteria => criteria != null)
                .ToList()
                .ForEach(criteria => request.SetQueryParams(criteria.GetCriteria()));

            return await request
                .AppendPathSegment(method)
                .GetJsonAsync<TType>();
        }
    }
}
