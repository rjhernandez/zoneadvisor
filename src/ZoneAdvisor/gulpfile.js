﻿/// <binding BeforeBuild='default' />
var gulp = require('gulp'),
  Cachebuster = require('gulp-cachebust'),
  concat = require('gulp-concat'),
  inject = require('gulp-inject'),
  install = require('gulp-install'),
  Promise = require('bluebird'),
  rimraf = Promise.promisify(require('rimraf')),
  runSequence = require('run-sequence');

var cachebust = new Cachebuster();

var wwwroot = './wwwroot/';

gulp.task('clean', function(cb) {

  Promise
    .join(
      rimraf(wwwroot + 'index.html'),
      // rimraf(wwwroot + 'lib'),
      rimraf(wwwroot + 'js/**/*.*'),
      rimraf(wwwroot + 'app/**/*.*'),
      function() {
        cb();
      });
});

gulp.task('copy:index', function () {
  return gulp.src(['Client/index.html'])
    .pipe(gulp.dest(wwwroot));
});

gulp.task('copy:html', function() {
  return gulp.src([
      'Client/app/views/*.html', 'Client/app/views/**/*.html',
      'Client/app/partials/*.html', 'Client/app/partials/**/*.html'])
    .pipe(gulp.dest(wwwroot + "app/views"));
});

gulp.task('build:js', function() {
  return gulp.src(['Client/app/**/*.js'], {
      base: "."
    })
    .pipe(concat(wwwroot + '/js/bundle.min.js'))
    .pipe(cachebust.resources())
    //.pipe(uglify())
    .pipe(gulp.dest("."));
});

gulp.task('build:css', function() {
  // Nothing yet.
});

gulp.task('inject', ["copy:html", "build:js", "build:css"], function() {
  var target = gulp.src(wwwroot + '/index.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths:
  var sources = gulp.src([wwwroot + '/js/**/*.js', './content/css/**/*.css'], {read: false});

  return target.pipe(inject(sources, { relative: true }))
    .pipe(gulp.dest(wwwroot));
});

gulp.task('bower', function () {
  return gulp.src(["./bower.json"])
      .pipe(install());
})


gulp.task('default', function () {
  runSequence([ 'clean', 'bower',
    'copy:index', 'copy:html', 'build:css',
    'build:js', 'inject']);
});
