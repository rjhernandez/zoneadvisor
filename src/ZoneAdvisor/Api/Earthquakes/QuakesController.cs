﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZoneAdvisor.Earthquakes;
using ZoneAdvisor.Earthquakes.Models;

namespace ZoneAdvisor.Api.Earthquakes
{
    [Route("api/[controller]")]
    public class QuakesController : Controller
    {
        private readonly IUsgs _usgs;

        public QuakesController(IUsgs usgs)
        {
            _usgs = usgs;
        }

        public async Task<IActionResult> Query([FromBody] BBox bbox)
        {
            var quakes = await _usgs.Query(bbox, new Magnitude {MagnitudeCriteria = MagnitudeCriteria.Five});
            return Json(quakes);
        }
    }
}
