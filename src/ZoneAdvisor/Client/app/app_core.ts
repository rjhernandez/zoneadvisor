﻿namespace ZoneAdvisor {
  "use strict";

  angular
    .module("ZoneAdvisor", ["ngMaterial", "ngRoute"])
    .run(["$log", "$rootScope", ($log:ng.ILogService, $rootScope:ng.IRootScopeService) => {

      $log.debug("[ZoneAdvisor] Application Started");
      
      $rootScope
        .$on('$routeChangeSuccess', (event, current, previous): void => {
          $log.debug(current);
        });
    }]);
}
