(function () {
    "use strict";
    angular
        .module("ZoneAdvisor")
        .config(config);
    config.$inject = ["$locationProvider", "$routeProvider"];
    function config($locationProvider, $routeProvider) {
        console.log("Router!");
        $routeProvider
            .when("/", {
            templateUrl: "/app/views/home.html",
            controller: "ZoneAdvisor.HomeController",
            controllerAs: "vm"
        })
            .when("/about", {
            templateUrl: "/app/views/about.html",
            controller: "ZoneAdvisor.AboutController",
            controllerAs: "vm"
        })
            .otherwise({
            redirectTo: "/"
        });
    }
})();
