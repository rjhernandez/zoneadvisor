var ZoneAdvisor;
(function (ZoneAdvisor) {
    "use strict";
    angular
        .module("ZoneAdvisor", ["ngMaterial", "ngRoute"])
        .run(["$log", "$rootScope", function ($log, $rootScope) {
            $log.debug("[ZoneAdvisor] Application Started");
            $rootScope
                .$on('$routeChangeSuccess', function (event, current, previous) {
                $log.debug(current);
            });
        }]);
})(ZoneAdvisor || (ZoneAdvisor = {}));
