namespace ZoneAdvisor {
  "use strict";


  class LayoutController {
    private msg1: string;

    static $inject = ["$log", "$scope"];

    constructor(
      private $log: ng.ILogService,
      private $scope: ng.IScope) {
      $log.info("[ZoneAdvisor.LayoutController] Instantiated.");
    }
  }

  angular
    .module("ZoneAdvisor")
    .controller("ZoneAdvisor.LayoutController", LayoutController);
}
