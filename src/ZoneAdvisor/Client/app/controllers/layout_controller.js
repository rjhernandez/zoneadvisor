var ZoneAdvisor;
(function (ZoneAdvisor) {
    "use strict";
    var LayoutController = (function () {
        function LayoutController($log, $scope) {
            this.$log = $log;
            this.$scope = $scope;
            $log.info("[ZoneAdvisor.LayoutController] Instantiated.");
        }
        LayoutController.$inject = ["$log", "$scope"];
        return LayoutController;
    }());
    angular
        .module("ZoneAdvisor")
        .controller("ZoneAdvisor.LayoutController", LayoutController);
})(ZoneAdvisor || (ZoneAdvisor = {}));
