namespace ZoneAdvisor {
  "use strict";
  
  class HomeController {

    public msg1: string;
    public msg2: string;

    static $inject = ["$log","$scope"];

    constructor(
      private $log: ng.ILogService,
      private $scope: ng.IScope) {

      $log.info("[ZoneAdvisor.HomeController] Instantiated.");

      this.msg1 = "Message 1";
      this.msg2 = "Message 2";
    }
  }

  angular
    .module("ZoneAdvisor")
    .controller("ZoneAdvisor.HomeController", HomeController);

}
