var ZoneAdvisor;
(function (ZoneAdvisor) {
    "use strict";
    var HomeController = (function () {
        function HomeController($log, $scope) {
            this.$log = $log;
            this.$scope = $scope;
            $log.info("[ZoneAdvisor.HomeController] Instantiated.");
            this.msg1 = "Message 1";
            this.msg2 = "Message 2";
        }
        HomeController.$inject = ["$log", "$scope"];
        return HomeController;
    }());
    angular
        .module("ZoneAdvisor")
        .controller("ZoneAdvisor.HomeController", HomeController);
})(ZoneAdvisor || (ZoneAdvisor = {}));
