﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Xunit;
using ZoneAdvisor.Earthquakes.Models;

namespace ZoneAdvisor.Earthquakes.Tests
{
    public class UsgsTests
    {
        private readonly EarthquakeSettings _earthquakeSettings;

        public UsgsTests()
        {
            var builder = new ConfigurationBuilder();
            var config = builder
                .AddJsonFile("./appsettings.json")
                .Build();

            _earthquakeSettings = new EarthquakeSettings { Usgs = config["Earthquakes:Usgs"] };
        }

        [Fact]
        public async Task Can_query_for_earthquakes()
        {
            var usgs = new Usgs(_earthquakeSettings);
            var quakes = await usgs.Query();
            Console.WriteLine($"Quakes (Last 30 Days) := {quakes.Features.Count}");
            Assert.NotNull(quakes);
        }

        [Fact]
        public async Task Can_query_for_earthquakes_magnitude_5()
        {
            var usgs = new Usgs(_earthquakeSettings);
            var quakes = await usgs.Query(
                new Magnitude { MagnitudeCriteria = MagnitudeCriteria.Five });
            Console.WriteLine($"Quakes (Last 30 Days, Magnitude 5) := {quakes.Features.Count}");
            Assert.NotNull(quakes);
        }
    }
}
