﻿using System.Linq;
using FluentAssertions;
using GeoJSON.Net.Feature;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using ZoneAdvisor.Api.Earthquakes;
using ZoneAdvisor.Earthquakes;
using System.Threading.Tasks;
using ZoneAdvisor.Earthquakes.Models;

namespace ZoneAdvisor.Api.Tests.Quakes
{
    public class QuakesControllerTests
    {
        [Fact]
        public async Task Query_delegates_work_to_IUsgs()
        {
            // Arrange.
            var expected = new FeatureCollection(Enumerable.Empty<Feature>().ToList());
            var usgs = new Mock<IUsgs>(MockBehavior.Strict);
            usgs.Setup(o => o.Query(It.IsAny<IUsgsCriteria>(),It.IsAny<Magnitude>())).ReturnsAsync(expected);
            var controller = new QuakesController(usgs.Object);
            
            // Act.
            var result = await controller.Query(null);

            // Assert.
            result.Should()
                .BeOfType<JsonResult>()
                .Which
                .Value
                .Should().NotBeNull()
                .Should().Be(expected);
        }
    }
}
