# install required components
nuget.exe install OpenCover -OutputDirectory .\packages -Version 4.6.519
nuget.exe install ReportGenerator -OutputDirectory .\packages -Version 2.4.4.0

# delete existing code coverage data.
if (Test-Path .coverage){
	Remove-Item .coverage -Force -Recurse
}
New-Item -Name .coverage -ItemType Directory -Force

# run tests
.\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe -target:"dotnet.exe" "-targetargs:test ${pwd}\test\ZoneAdvisor.Tests\project.json" -output:.\.coverage\coverage.api.xml -register:user -filter:+[ZoneAdvisor.*]*
.\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe -target:"dotnet.exe" "-targetargs:test ${pwd}\test\ZoneAdvisor.Earthquakes.Tests\project.json" -output:.\.coverage\coverage.client.xml -register:user -filter:+[ZoneAdvisor.*]*

# prepare reports.
.\packages\ReportGenerator.2.4.4.0\tools\ReportGenerator.exe -reports:"${pwd}\.coverage\coverage.*.xml" -targetDir:"${pwd}\.coverage\report" -reportTypes:Html